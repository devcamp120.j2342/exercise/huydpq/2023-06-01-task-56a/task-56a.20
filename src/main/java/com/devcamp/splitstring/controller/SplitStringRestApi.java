package com.devcamp.splitstring.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@CrossOrigin
public class SplitStringRestApi {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(required = true, name = "string") String requString) {
        ArrayList<String> splitStringArr = new ArrayList<>();

        String [] stringArr = requString.split(" ");

        for (String stringElement : stringArr) {
                splitStringArr.add(stringElement);
        }

        return splitStringArr;
    }
}
